#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void lash_loop(void)
{
  char *line;
  char **args;
  int status;

  do {
    printf("λ ");
    line = lash_read_line();
    args = lash_split_line(line);
    status = lash_execute(args);

    free(line);
    free(args);
  } while (status);
}

#define LASH_RL_BUFSIZE 2048
char *lash_read_line(void)
{
  char *line = NULL;
  ssize_t bufsize = 0;

  if (getline(&line, &bufsize, stdin) == -1){
    if (feof(stdin)) {
      exit(EXIT_SUCCESS);
    } else  {
      perror("readline");
      exit(EXIT_FAILURE);
    }
  }

  return line;
}

#define LASH_TOK_BUFSIZE 64
#define LASH_TOK_DELIM " \t\r\n\a"
char **lash_split_line(char *line)
{
  int bufsize = LASH_TOK_BUFSIZE, position = 0;
  char **tokens = malloc(bufsize * sizeof(char*));
  char *token;

  if (!tokens) {
    fprintf(stderr, "lash: allocation error\n");
    exit(EXIT_FAILURE);
  }

  token = strtok(line, LASH_TOK_DELIM);
  while (token != NULL) {
    tokens[position] = token;
    position++;

    if (position >= bufsize) {
      bufsize += LASH_TOK_BUFSIZE;
      tokens = realloc(tokens, bufsize * sizeof(char*));
      if (!tokens) {
        fprintf(stderr, "lash: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }

    token = strtok(NULL, LASH_TOK_DELIM);
  }
  tokens[position] = NULL;
  return tokens;
}

int lash_launch(char **args)
{
  pid_t pid, wpid;
  int status;

  pid = fork();
  if (pid == 0) {
    if (execvp(args[0], args) == -1) {
      perror("lash");
    }
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
    perror("lash");
  } else {
    do {
      wpid = waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }

  return 1;
}

int lash_cd(char **args);
int lash_help(char **args);
int lash_exit(char **args);

char *builtin_str[] = {
  "cd",
  "help",
  "exit"
};

int (*builtin_func[]) (char **) = {
  &lash_cd,
  &lash_help,
  &lash_exit
};

int lash_num_builtins() {
  return sizeof(builtin_str) / sizeof(char *);
}

int lsh_cd(char **args)
{
  if (args[1] == NULL) {
    fprintf(stderr, "lash: expected argument to \"cd\"\n");
  } else {
    if (chdir(args[1]) != 0) {
      perror("lash");
    }
  }
  return 1;
}

int lash_help(char **args)
{
  int i;
  printf("The following are built in:\n");

  for (i = 0; i < lash_num_builtins(); i++) {
    printf("  %s\n", builtin_str[i]);
  }

  printf("Use the man command for information on other programs.\n");
  return 1;
}

int lash_exit(char **args)
{
  return 0;
}

int lash_execute(char **args)
{
  int i;

  if (args[0] == NULL) {
    return 1;
  }

  for (i = 0; i < lash_num_builtins(); i++) {
    if (strcmp(args[0], builtin_str[i]) == 0) {
      return (*builtin_func[i])(args);
    }
  }

  return lash_launch(args);
}

int main(int argc, char **argv)
{
  lash_loop();
  return EXIT_SUCCESS;
}